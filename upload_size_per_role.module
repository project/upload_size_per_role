<?php

/**
 * @file
 * Contains hook implementations for the upload size per user role module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Component\Utility\Bytes;

/**
 * Implements hook_form_alter().
 */
function upload_size_per_role_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $current_user = \Drupal::currentUser();
  $current_user_roles = $current_user->getRoles();
  $upload_max = Bytes::toNumber(ini_get('upload_max_filesize')) / 1024 / 1024;

  if ($form_state->getFormObject() instanceof EntityFormInterface) {
    $entity_type = $form_state->getFormObject()->getEntity()->getEntityTypeId();
    $bundle = $form_state->getFormObject()->getEntity()->bundle();

    $row_key = $entity_type . '_' . $bundle . '_';
    $mapping = [];
    foreach (\Drupal::service('config.factory')->getEditable('upload_size_per_role.settings')->get('mapping') as $key => $value) {
      foreach ($value as $map_key => $map) {
        if (str_starts_with($map_key, $row_key) && !empty($map)) {
          $mapping[$map_key] = $map;
        }
      }
    }
    
    $final_map = [];
    $field_list = [];
    foreach ($current_user_roles as $user_role_key => $user_role) {
      foreach ($mapping as $map_key => $map_value) {
        if (str_ends_with($map_key, $user_role)) {
          $final_map[$map_key] = $mapping[$map_key];
          $field_id = str_replace($row_key, '', str_replace('_' . $user_role, '', $map_key));

          if (isset($field_list[$field_id]) && $field_list[$field_id] < $map_value) {
            $field_list[$field_id] = $map_value;
          }
          else {
            $field_list[$field_id] = $map_value;
          }
        }
      }
    }

    foreach ($field_list as $key => $filed) {
      if ($upload_max < $filed) {
        $filed = $upload_max;
      }

      $pattern = '/\d+/';
      $replacement = $filed;
      $form[$key]['widget'][0]['#description'] = preg_replace($pattern, $replacement, $form[$key]['widget'][0]['#description']);
      if (isset($form[$key]['widget'][0]['#upload_validators']['FileSizeLimit']['fileLimit'])) {
        $form[$key]['widget'][0]['#upload_validators']['FileSizeLimit']['fileLimit'] = Bytes::toNumber("$filed MB");
      }

      if (isset($form[$key]['widget'][0]['#upload_validators']['file_validate_size'][0])) {
        $form[$key]['widget'][0]['#upload_validators']['file_validate_size'][0] = Bytes::toNumber("$filed MB");
      }
    }
  }
}
