<?php

namespace Drupal\upload_size_per_role\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Component\Utility\Bytes;

/**
 * Class Upload size per role settings form.
 */
class UploadSizePerRoleSettingsForm extends ConfigFormBase {

  /**
   * The entity field manager.
   */
  protected $entityFieldManager;

  /**
   * Php max upload size.
   */

  Public $uploadMax;

  /**
   * Constructs a LanguageSuggestionMappingForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManager $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityFieldManager = $entity_field_manager;
    $this->uploadMax = Bytes::toNumber(ini_get('upload_max_filesize')) / 1024 / 1024;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['upload_size_per_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'upload_size_per_role_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<i>' . $this->t('Enter the values in MB') . '</i>';
    $mapping = [];
    if (!empty($this->config('upload_size_per_role.settings')->get('mapping'))) {
      foreach ($this->config('upload_size_per_role.settings')->get('mapping') as $key => $value) {
        foreach ($value as $map_key => $map) {
          $mapping[$map_key] = $map;
        }
      }
    }

    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    $field_map = $this->entityFieldManager->getFieldMap();

    $user_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();

    foreach ($entity_types as $key => $entity_type) {
      if ($entity_type->entityClassImplements(FieldableEntityInterface::class)) {
        $fields = $this->entityFieldManager->getFieldStorageDefinitions($key);

        foreach ($fields as $field_key => $field) {
          if ($field->getType() == 'file') {
            $supported_field[$key][$field_key] = $field;
          }
        }
      }
    }

    foreach ($supported_field as $entity_type => $fields) {
      $entityStorage = \Drupal::entityTypeManager()->getStorage($entity_type);

      $form[$entity_type] = [
        '#type' => 'fieldset',
        '#title' => $entityStorage->getEntityType()->getLabel()->__toString(),
      ];

      if (!empty($fields)) {
        $rows = [];

        $form[$entity_type]['mapping'] = [
          '#type' => 'table',
          '#header' => [
            'field' => $this->t('Field'),
            'type' => $this->t('Type'),
            'size' => $this->t('Maximum upload size'),
          ],
        ];

        foreach ($user_roles as $key => $value) {
          $form[$entity_type]['mapping']['#header'][$key] = $value->label();
        }

        foreach ($fields as $key => $field) {
          $bundles = $field_map[$entity_type][$key]['bundles'];
          foreach ($bundles as $bundle) {
            $field_dfinition = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

            $row = [
              'label' => ['#markup' => $field_dfinition[$key]->getLabel()],
              'bundle' => ['#markup' => $bundle],
              'file_size' => ['#markup' => $field_dfinition[$key]->getSettings()['max_filesize']],
            ];;

            foreach ($user_roles as $user_role) {
              $row_key = $entity_type . '_' . $bundle . '_' . $key . '_' . $user_role->id();
              $row[$row_key] = [
                '#type' => 'number',
                '#min' => 1,
                '#max' => $this->uploadMax,
                '#default_value' => $mapping[$row_key] ?? '',
              ];
            }
            $form[$entity_type]['mapping'][] = $row;
          }
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('upload_size_per_role.settings')
      ->set('mapping', $form_state->getValue('mapping'))
      ->save();
  }

}
